package internal

import (
	"context"
	"encoding/json"
	"fmt"
	"log"
	"time"

	pb "gitlab.com/vyntt/assignment2/api"
	"gitlab.com/vyntt/assignment2/pkg/ent"
	// "gitlab.com/vyntt/assignment2/pkg/ent/history"
	// "google.golang.org/protobuf/proto"
)

// Hub maintains the set of active clients and broadcasts messages to the
// clients.
type Hub struct {
	// Registered clients. ----All of clients
	clients map[*Client]bool
	channels map[string][]*Client

	// Inbound messages from the clients.
	broadcast chan []byte

	// Register requests from the clients.
	register chan *Client

	// Unregister requests from clients.
	unregister chan *Client

	db *ent.Client
	//Username of member hub
	// username map[string]bool

}

func NewHub(db *ent.Client) *Hub {
	return &Hub{
		db: db,
		broadcast:  make(chan []byte),
		register:   make(chan *Client),
		unregister: make(chan *Client),
		clients:    make(map[*Client]bool),
		// username: 	make(map[string]bool),
		channels: 	make(map[string][]*Client),
	}
}

func (h *Hub) Run() {
	for {
		select {
		case client := <-h.register:
			h.clients[client] = true
		case client := <-h.unregister:
			if _, ok := h.clients[client]; ok {
				delete(h.clients, client)
				close(client.send)
			}
		case message := <-h.broadcast:
			// Find the name of channel
			var m pb.Message	// json of message
			err1 := json.Unmarshal(message, &m)
			fmt.Println("Message: ", m.Username)

			_, errCreate := h.db.History.Create().SetContent(m.Content).SetSendAt(time.Now()).SetChannel(m.Channel).SetSender(m.Username).Save(context.Background())
			if errCreate != nil {
				log.Fatal("Fail to add message to db: ", errCreate)
			}
			if err1 != nil {
				log.Fatal("Fail to decode message in run: ", err1)
			}
			var channel = m.Channel
			var msg_pb = m.Content
			fmt.Printf("Type: %T\n", msg_pb)
			var msg, err2 = json.Marshal(msg_pb) // decode message of json
			if err2 != nil {
				log.Fatal("Fail to encode message in run: ", err2)
			}
			var clientsChannel = h.channels[channel] // find the list of clients 
			var n = len(clientsChannel)
			for i := 0; i < n; i++ {
				// if clientsChannel[i].username == userName {
				// 	continue
				// }
				select {
				case clientsChannel[i].send <- msg: //send message to the client
				default:
					close(clientsChannel[i].send)
					delete(h.clients, clientsChannel[i])
				}
			}
		}
	}
}