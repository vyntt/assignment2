package main

import (
	"flag"
	"fmt"
	"log"
	"net/http"
	"context"
	_ "github.com/go-sql-driver/mysql"
	"gitlab.com/vyntt/assignment2/pkg/ent"
	"gitlab.com/vyntt/assignment2/internal"
)

var (
	addr = flag.String("addr", ":8080", "http service address")
)

func serveHome(w http.ResponseWriter, r *http.Request) {
	log.Println(r.URL)
	fmt.Println(r.URL)
	if r.URL.Path != "/" {
		fmt.Println("error here")
		http.Error(w, "Not found", http.StatusNotFound)
	}
	if r.Method != http.MethodGet {
		http.Error(w, "Method not allowed", http.StatusMethodNotAllowed)
	}
	fmt.Println("HTML here")
	http.ServeFile(w, r, "./internal/home.html")
}

func main() {
	db, errDB := ent.Open("mysql", "root:TuVy2709@@tcp(localhost:13306)/history")
	if errDB != nil {
		log.Fatal("Fail to connect database: ", errDB)
	}
	defer db.Close()
	// Run the auto migration tool.
	if errSchema := db.Schema.Create(context.Background()); errSchema != nil {
		log.Fatalf("failed creating schema resources: %v", errSchema)
	}

	fmt.Println("Start web")
	flag.Parse()
	hub := internal.NewHub(db)
	go hub.Run()
	http.HandleFunc("/", serveHome)
	fmt.Println("Home page")
	http.HandleFunc("/ws", func(w http.ResponseWriter, r *http.Request) {
		fmt.Println("Go to Web Socket")
		internal.ServeWs(hub, w, r)
	})
	fmt.Println(*addr)
	err := http.ListenAndServe(*addr, nil)
	if err != nil {
		log.Fatal("ListenAndServe: ", err)
	}
}
