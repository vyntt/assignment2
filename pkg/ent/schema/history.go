package schema

import (
	"time"
	"entgo.io/ent"
	"entgo.io/ent/schema/field"
)

// History holds the schema definition for the History entity.
type History struct {
	ent.Schema
}

// Fields of the History.
func (History) Fields() []ent.Field {
	return []ent.Field{
		field.Time("send_at").Default(time.Now),
		field.String("channel").Default("unknown"),
		field.String("sender").Default("unknown"),
		field.String("content").Default(""),
	}
}

// Edges of the History.
func (History) Edges() []ent.Edge {
	return nil
}
